const express = require('express');
var cors = require('cors')
const app = express();
const port = 3001;
const oracledb = require('oracledb');

var ip = '45.32.119.244'
var password = '12345678'

var setting = {
    user: "gosoft14",
    password: password,
    connectString: ip
}

app.use(express.static('img'));
app.use('/img', express.static('img'))
app.use(cors())

app.get('/users', async function (req, res, next) {
    try {
        connection = await oracledb.getConnection(setting);
        var result = await connection.execute(`
        SELECT 
            *
        FROM
            EMPLOYEE
        LEFT OUTER JOIN JOB ON
            EMPLOYEE.JOB_ID = JOB.ID
        ORDER BY 
            EMPLOYEE.ID 
        `);
        return res.status(200).json({
            code: 1,
            message: 'OK',
            data: result
        })
    } catch (err) {
        console.error(err.message);
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.error(err.message);
            }
        }
    }
});

app.get('/jobid/:id', async function (req, res, next) {
    try {
        let id = req.params.id
        connection = await oracledb.getConnection(setting);
        var result = await connection.execute(`
        SELECT 
            * 
        FROM 
            JOB
        WHERE
            ID='${id}'
        `);
        return res.status(200).json({
            code: 1,
            message: 'OK',
            data: result
        })
    } catch (err) {
        console.error(err.message);
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.error(err.message);
            }
        }
    }
});

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`);
});
