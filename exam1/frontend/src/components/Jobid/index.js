import { useNavigate } from "react-router-dom";
import React, { useState } from 'react';

function Jobid() {

    const navigate = useNavigate()
    const [id, setID] = useState("");

    const handleSubmit = (e) => {
        e.preventDefault();
        navigate(`/job-no-one/${id}`)
    };

    const handleChange = (e) =>  {
        setID(e.target.value);
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <label>
                    Job ID:
                    <input type="text" pattern="[0-9]*" name="id" onChange={handleChange} />
                </label>
                <input type="submit" value="Submit" />
            </form>
        </div>
    )
}

export default Jobid;
