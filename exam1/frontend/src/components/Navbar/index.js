import { useNavigate } from "react-router-dom";

function Navbar () {
    
    const navigate = useNavigate()
    
    return (
        <div className="navbar">
            <div className="nav" onClick={() => navigate('/employee')}>Employee Data</div>
            <div className="nav" onClick={() => navigate('/job')}>Get Job ID</div>
        </div>
    )
}

export default Navbar;
