import React, { useEffect, useState } from 'react';

function Employee() {

    const [employeeData, setEmployee] = useState("")
    useEffect(() => {
        fetch(`http://localhost:3001/users`)
            .then(response => response.json())
            .then(data => setEmployee({
                data
            }));
    }, []);

    return (
        <div>
            {
                employeeData !== "" ? (
                    <table>
                        <tr>
                            <th>{employeeData.data.data.metaData[0].name}</th>
                            <th>{employeeData.data.data.metaData[1].name}</th>
                            <th>{employeeData.data.data.metaData[2].name}</th>
                            <th>{employeeData.data.data.metaData[3].name}</th>
                            <th>{employeeData.data.data.metaData[4].name}</th>
                            <th>{employeeData.data.data.metaData[5].name}</th>
                            <th>{employeeData.data.data.metaData[6].name}</th>
                        </tr>
                        {
                            employeeData.data.data.rows.map((employee) =>
                            <tr>
                                <th>{employee[0]}</th>
                                <th>{employee[1]}</th>
                                <th>{employee[2]}</th>
                                <th>{employee[3]}</th>
                                <th>{employee[4]}</th>
                                <th>{employee[5]}</th>
                                <th>{employee[6]}</th>
                            </tr>
                            )
                        }
                    </table>
                ) : (false)
            }
        </div>
    )
}

export default Employee;