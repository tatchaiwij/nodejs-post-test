import { useParams } from "react-router-dom";
import React, { useEffect, useState } from 'react';

function Jobnoone() {

  const { id } = useParams();

  const [jobData, setJob] = useState("")
  useEffect(() => {
    fetch(`http://localhost:3001/jobID/${id}`)
      .then(response => response.json())
      .then(data => setJob({
        data
      }));
  }, []);

  return (
    <div>
      {
        jobData !== "" ? (
          <table>
            <tr>
              <th>{jobData.data.data.metaData[0].name}</th>
              <th>{jobData.data.data.metaData[1].name}</th>
              <th>{jobData.data.data.metaData[2].name}</th>
            </tr>
            <tr>
              <td>{jobData.data.data.rows[0][0]}</td>
              <td>{jobData.data.data.rows[0][1]}</td>
              <td>{jobData.data.data.rows[0][2]}</td>
            </tr>
          </table>
        ) : (false)
      }
    </div>
  )
}

export default Jobnoone;
