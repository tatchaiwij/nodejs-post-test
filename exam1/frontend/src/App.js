import './App.css';
import Employee from "./components/Employee";
import Jobid from "./components/Jobid";
import Jobnoone from "./components/Jobnoone";
import Navbar from './components/Navbar';

import { BrowserRouter, Route, Routes } from 'react-router-dom';

function App() {
  return (
    <div>
     <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path='/employee' index element={<Employee />} />
          <Route path='/job' element={<Jobid />} />
          <Route path='/job-no-one/:id' element={<Jobnoone />} />
        </Routes>
     </BrowserRouter>
   </div>
  );

}

export default App;
