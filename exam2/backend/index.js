const Hapi = require('@hapi/hapi');
const { payload } = require('@hapi/hapi/lib/validation');
const Jwt = require('@hapi/jwt');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const oracledb = require('oracledb');

var ip = '45.32.119.244'
var password = '12345678'

var setting = {
    user: "gosoft14",
    password: password,
    connectString: ip
}

const init = async () => {
    const server = new Hapi.Server({
        port: 3001,
        host: 'localhost'
    });
    await server.register(Jwt);
    server.auth.strategy('my_jwt_stategy', 'jwt', {
        keys: 'some_shared_secret',
        verify: {
            aud: 'urn:audience:test',
            iss: 'urn:issuer:test',
            sub: false,
            nbf: true,
            exp: true,
            maxAgeSec: 14400,
            timeSkewSec: 15
        },
        validate: (artifacts, request, h) => {
            var respond = await oracleQuery(`
                SELECT 
                    * 
                FROM 
                    USER 
                WHERE 
                    USER.USERID='${artifacts.decoded.payload.payload.userID}'`);
            if (respond.data.rows !== [])
                if (respond.data.rows[3] == artifacts.decoded.payload.payload.password)
                    return {
                        isValid: true,
                        credentials: {
                            userID: artifacts.decoded.payload.payload.userID,
                            password: artifacts.decoded.payload.payload.password
                        }
                    };
                else
                    return {
                        isValid: false
                    }
        }
    });
    server.route({
        method: 'PUT',
        path: '/gettoken',
        config: {
            cors: true,
            handler(request, h) {
                const payload = request.payload;
                var respond = await oracleQuery(`
                SELECT 
                    SALT 
                FROM 
                    USER 
                WHERE 
                    USER.USERID='${payload.userID}'`);
                const salt = respond.data.rows[0]
                request.payload.password = bcrypt.hashSync(request.password, salt);
                const token = jwt.sign({
                    payload,
                    aud: 'urn:audience:test',
                    iss: 'urn:issuer:test',
                    sub: false,
                    maxAgeSec: 14400,
                    timeSkewSec: 15
                }, 'some_shared_secret');
                var data = {
                    token: token
                }
                return data;
            },
        }
    });

    server.route({
        method: 'POST',
        path: '/register',
        config: {
            cors: true,
            handler(request, h) {
                const payload = request.payload;
                const salt = bcrypt.genSaltSync(10);
                var hash = bcrypt.hashSync(payload.password, salt);
                try {
                    connection = await oracledb.getConnection(setting);
                    result = await connection.execute("INSERT INTO USER VALUES (:name, :user_id, :password, :salt)",
                        {
                            name: payload.name,
                            user_id: payload.userID,
                            password: hash,
                            salt: salt,
                        }, { autoCommit: true });
                } catch (err) {
                    console.error(err.message);
                } finally {
                    if (connection) {
                        try {
                            await connection.close();
                        } catch (err) {
                            console.error(err.message);
                        }
                    }
                }    
            },
        }
    });

    server.route({
        method: 'GET',
        path: '/login',
        config: {
            cors: true,
            handler(request, h) {
                var data = {
                    message: `Complete`
                }
                return data;
            },
            auth: {
                strategy: 'my_jwt_stategy',
            }
        }
    });
    await server.start();
    console.log('Server running at:', server.info.uri);
};

async function oracleQuery(query) {
    try {
        connection = await oracledb.getConnection(setting);
        result = await connection.execute(query);
        return result
    } catch (err) {
        console.error(err.message);
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.error(err.message);
            }
        }
    }
}


process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});
init();
